#!/usr/bin/env python
#   Copyright 2012 Gary Wright http://identi.ca/wrightsolutions
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

# python /tmp/cheetah-vhost-limitgetpostput.py
# Would attempt to use a template named vhost-limitgetpostput.tpl
# tpl on the filesystem. tmpl commonly used in variable names.

from Cheetah.Template import Template
from sys import argv
from os.path import basename,dirname,isfile

cheetah_template_extension = 'tpl'	# tmpl or tpl as you prefer

def tmpl_from_scriptname():
	t_found = None
	script_basename = basename(__file__)
	script_dirname = dirname(__file__)
	pos = script_basename.index('cheetah-')
	if pos == 0:
		t_basename = script_basename[8:].split('.')[0] + \
			'.' + cheetah_template_extension
		t_samedir = script_dirname + '/' + t_basename
		t_otherdir = '/var/lib/cheetah/' + t_basename
		if isfile(t_basename):
			t_found = t_basename
		elif isfile(t_samedir):
			t_found = t_samedir
		elif isfile(t_otherdir):
			t_found = t_otherdir
	return t_found

tmpl = tmpl_from_scriptname()
#print tmpl_from_scriptname()
if tmpl:
	directory_list = argv[1:]	# argv[1:-1]
	print tmpl_from_scriptname()
	for dir in directory_list:
		#print dir
		t=Template(file=tmpl)
		t.directory = dir.rstrip(chr(47))
		print str(t)
		print "\n"

