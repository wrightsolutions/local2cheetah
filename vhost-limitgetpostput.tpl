<Directory "$directory">
  <Limit GET>
    Order allow,deny
    Allow from all
    </Limit>
  <Limit POST PUT>
    Order allow,deny
    #Allow from all
    Deny from all
  </Limit>
  <LimitExcept GET POST PUT>
    Order allow,deny
    Deny from all
  </LimitExcept>
  Options -Indexes
</Directory>
